#!/bin/bash
#
#    Copyright (C) 2009,2010,2011  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#
#
#
#    This script compares two Sources.gz files, and reports available updates

if [ $(git status -s | wc -l) != 0 ]
then
    echo You have uncommited git changes, stopping
    [ 1$1 = 1"test" ] || exit 1
fi

PAIRS="taranis-lucid toutatis-precise belenos-trusty"
CODENAMES="taranis toutatis belenos"
UPSTREAMS="lucid precise trusty"

[ 1$1 = 1"test" ] && TEST=echo

WD=$PWD
LOGS=$PWD/logs
[ -d $LOGS ] || mkdir $LOGS
export LANG=C
FILE=$(mktemp)
TMP=$(mktemp -d)
cd $TMP

echo "List of packages with available updates:
" > $FILE

listmirror (){
DIST=$1
COMPONENT=$2
echo -n Downloading Sources.gz from $MIRROR/dists/$DIST/$COMPONENT/source/Sources.gz >&2
wget $MIRROR/dists/$DIST/$COMPONENT/source/Sources.gz -q -O - | zcat | egrep dsc$ | cut -d" " -f 4|sed 's/\.dsc//'
echo " Done" >&2
}

update(){

MIRROR="http://archive.ubuntu.com/ubuntu"

for i in $UPSTREAMS
do
    listmirror $i main >> $i
    listmirror $i-updates main >> $i
    listmirror $i-security main >> $i
    listmirror $i universe >> $i
    listmirror $i-updates universe >> $i
    listmirror $i-security universe >> $i
done

for i in  $CODENAMES
do
    MIRROR="http://archive.trisquel.info/trisquel"
    listmirror $i main >> $i
    listmirror $i-updates main >> $i
    listmirror $i-security main >> $i

    MIRROR="http://devel.trisquel.info/trisquel/$i"
    listmirror $i main >> $i
done

}

higher(){
LIST="$(echo $* |sed 's/:/./g; s/+/./g') EOL"

HI=$(echo $LIST | cut -d" " -f1)
LIST=$(echo $LIST | cut -d" " -f2-)

while [ "$LIST" != "EOL" ]
do
    TEST=$(echo $LIST | cut -d" " -f1)
    dpkg --compare-versions $HI lt $TEST 2>/dev/null && HI=$TEST
    LIST=$(echo $LIST | cut -d" " -f2-)
done
echo $HI
}

check(){
for package in $PACKAGES
do
LIST=$(egrep ^${package}_ $DISTRO)
current=$( higher $LIST)
LIST=$(egrep ^${package}_ $UPSTREAM)
upstream=$( higher $LIST)

if dpkg --compare-versions $current lt $upstream 2>/dev/null
then

 #   echo Current version in $DISTRO: $current 1>&2
 #   echo Upstream version in $UPSTREAM: $upstream 1>&2

    echo Current version in $DISTRO: $current
    echo Upstream version in $UPSTREAM: $upstream
    [ $current = "EOL" ] && continue
    if [ $package = "grub2" ]
    then
        echo WARNING! grub2 packages need to be manually compiled!
        echo
        continue
    fi

    cd $WD
    $TEST git checkout $DISTRO
    $TEST git merge master
    cd $TMP
    echo Upgrading $current at $DISTRO with ${UPSTREAM}_$upstream 1>&2
    echo Running makepackage $package $DISTRO at $(date) $LOGS/$DISTRO-$package-$(date +%Y%m%d).log
    echo Running makepackage $package $DISTRO at $(date) $LOGS/$DISTRO-$package-$(date +%Y%m%d).log 1>&2
    $TEST sh $WD/makepackage $package $DISTRO > $LOGS/$DISTRO-$package-$(date +%Y%m%d).log && echo -n Done! || echo -n FAILED!
    echo " ($(date))"
    echo
fi
done
}

update

for pair in $PAIRS
do
DISTRO=${pair/-*/}
UPSTREAM=${pair/*-/}
PACKAGES=$(git --git-dir=/home/systems/devel/.git ls-tree -r --name-only $DISTRO|grep helpers/make-|sed 's/.*make-//')
check >> $FILE
done

report () {
#address=trisquel-devel@listas.trisquel.info
address=ruben@trisquel.info

echo "Subject: Compiling updates
To: $address

$(cat $FILE)"  | $TEST /usr/sbin/sendmail -f watchdog@devel.trisquel.info -h correo.sognus.com -t $address
}

grep -q makepackage $FILE && $TEST report
rm $FILE $TMP -rf
